#!/usr/bin/python3

from random import uniform
from time import sleep
import sys
import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def typewriter_print(str):
    for ch in str:
        print(ch, end='', flush=True)
        sleep(uniform(0, 0.1))

def wait_msg():
    typewriter_print("Для продолжения нажмите Enter...")
    input()
    cls()


user_score = 0
ai_score = 0

welcome_msg = """Добро пожаловать на тренировку!

Ваша задача одолеть нашу тренировочную программу.
Победа присуждается тому, кто первым наберет 2 очка.

"""

sorry_msg = """Прости, я чет не понял.
Последнее время осознаю только односложные прямые ответы.
Так ты пойдешь катать?
"""

hire_msg = """Привет!
Пойдешь в доту катать?
"""

cls()
typewriter_print(welcome_msg)

while True:
    if user_score == 2:
        typewriter_print("Поздравляем! Вы скала.\n")
        typewriter_print("Итоговый счет " + str(user_score) + ":" + str(ai_score) + "\n\n")
        wait_msg()
        break
    elif ai_score == 2:
        typewriter_print("Вы немощь!\n")
        typewriter_print("Итоговый счет " + str(user_score) + ":" + str(ai_score) + "\n\n")
        wait_msg()
        break

    typewriter_print("Текущий счет " + str(user_score) + ":" + str(ai_score) + "\n")
    wait_msg()

    typewriter_print(hire_msg)
    while True:
        answer = input()
        if(answer.lower() == "да"):
            ai_score += 1
            cls()
            break
        elif(answer.lower() == "нет"):
            user_score += 1
            cls()
            break
        typewriter_print(sorry_msg)
    
